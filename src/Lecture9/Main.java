package Lecture9;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map<String, String> array = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        });
        array.put("NG", "31.12");
        array.put("Den surka", "22.06");
        array.put("Rozhdestvo", "14.01");
        array.put("Den konstitucii", "30.08");
        array.put("1 sentyabrya", "01.09");

        Iterator<Map.Entry<String, String>> iterator = array.entrySet().iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

        System.out.println("----");
        for (Map.Entry<String, String> stringStringEntry : array.entrySet()) {
            System.out.println(stringStringEntry);
        }

        System.out.println("----");
        System.out.println(array.entrySet());

    }
}
